# Laravel Currency List

## Requirements

-   PHP 8.1
-   Nodejs 18.13

## Setup

### Laravel

To install Laravel dependencies, run

```bash
$ composer install
```

For convenience, the .env file is provided and the database used is Sqlite. The database has already been seeded, however, you can reseed with

```bash
$ php artisan migrate:fresh --seed
```

You will need to be online, as it uses an API to get the currency list and rates.

### Node

To install Node dependencies, run

```bash
$ npm install
```

You may run the build step with

```bash
$ npm run build
```

### Running

Finally, you can run the web app using the Laravel built-in server using:

```bash
$ php artisan serve
```

Then navigate to localhost:8000 on your browser.

## Usage

### Currencies

A list of the currencies is available under the Currencies link. From there, you can add a currency to your portfolio and update the currency rates.

The date may be a day behind as the API's timezone may be different from yours.

### Portfolio

The portfolio is where you can add the currencies you are interested in. To add a currency, click on **Add Currency**, when in the Portfolio listing, or **Add to Portfolio** when in the Currency listing.

When adding a currency, you need to select a currency from the select/dropdown, otherwise you'll get validation error. You also cannot add a duplicate currency, this will also cause a validation error.

You can edit a currency by clicking on the **Edit Action** on the currency in the Portfolio listing or in the Currency listing. You may change the currency and/or the notes. The same validation rules apply - the currency must be chosen and must not already exist on in the Portfolio.

Finally, you may delete a currency from the Portfolio by clicking on the **Delete Action** in the Portfolio listing or on the **Delete from Portfolio Action** in the Currency listing.

## Tests

You may run all the tests in the test suite by running

```bash
$ composer test
```

from the root directory. Tests are located in the tests/Feature folder. There are two, **CurrencyTest**, which tests downloading and refreshing the currency, and **PortfolioTest**, which tests all the Portfolio CRUD operations.

## Information

### API used

-   https://github.com/fawazahmed0/exchange-api

### Stack used

-   Laravel/Blade (backend/frontend)
-   Livewire (ajax forms, datatables)
-   Blade UI Icons (frontend icons)
-   Ripple UI (frontend components - mostly the forms/buttons)
-   Laravel Sweetalert2 (toast notifications)
-   Tailwind CSS
-   Sqlite (database)
-   Neovim (IDE)
