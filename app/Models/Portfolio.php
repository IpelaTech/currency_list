<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Portfolio extends Model
{
    use HasUuids;
    use HasFactory;

    protected $guarded = [];

    public static function search(string $search)
    {
        if (empty($search)) {
            return static::query();
        }

        return static::query()->whereHas("currency", function (Builder  $query) use ($search) {
            $query->where("name", "LIKE", "%{$search}%")
            ->orWhere("code", "LIKE", "%{$search}%");
        });
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, "currency_id");
    }
}
