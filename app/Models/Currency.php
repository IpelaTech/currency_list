<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Currency extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        "rate_date" => "date"
    ];

    public static function search(string $search)
    {
        if (empty($search)) {
            return static::query();
        }

        return static::query()->where("name", "LIKE", "%{$search}%")
            ->orWhere("code", "LIKE", "%{$search}%");
    }

    public function portfolios(): HasMany
    {
        return $this->hasMany(Portfolio::class, "currency_id");
    }
}
