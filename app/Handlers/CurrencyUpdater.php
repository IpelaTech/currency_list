<?php

namespace App\Handlers;

use App\Models\Currency;
use Illuminate\Support\Facades\Http;

class CurrencyUpdater
{
    public static function update()
    {
        $currency_rate_response = Http::get(config("currency.url_zar"));

        if ($currency_rate_response->ok()) {
            $rates = $currency_rate_response->json();

            $date = $rates["date"];
            foreach ($rates["zar"] as $code => $rate) {
                $currency = Currency::where("code", $code)->first();
                $currency->update([
                    "rate" => $rate,
                    "rate_date" => $date
                ]);
            }
        }
    }
}
