<?php

namespace App\Handlers;

use App\Models\Currency;
use Illuminate\Support\Facades\Http;

class CurrencyDownloader
{
    public static function download()
    {
        $currency_list_response = Http::get(config("currency.url_list"));
        $currencies = [];

        if ($currency_list_response->ok()) {
            $currencies = $currency_list_response->json();

            foreach ($currencies as $code => $currency) {
                Currency::create([
                    "name" => $currency,
                    "code" => $code
                ]);
            }
        }

        return $currencies;
    }
}
