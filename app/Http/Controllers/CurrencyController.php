<?php

namespace App\Http\Controllers;

use App\Handlers\CurrencyUpdater;
use App\Models\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CurrencyController extends Controller
{
    public function index()
    {
        $latest_date = Currency::first()->rate_date;

        $currencies =  Currency::with(["portfolios"])->get();

        return view("currencies.index", [
            "currencies" => $currencies,
            "latest_date" => $latest_date
        ]);
    }

    public function store()
    {
        CurrencyUpdater::update();
        alert()->success("success", "Currency rates updated");

        return redirect()->route("currencies.index");
    }
}
