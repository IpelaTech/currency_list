<?php

namespace App\Http\Controllers;

use App\Handlers\CurrencyUpdater;
use App\Models\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index()
    {
        return view(
            "portfolio.index",
            [
                "portfolios" => Portfolio::with("currency")->get()
            ]
        );
    }

    public function create()
    {
        $currency = request()->query("currency");

        if (isset($currency)) {
            return view("portfolio.create", [
                "currency" => $currency
            ]);
        }

        return view("portfolio.create");
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            "currency_id" => "required|integer|exists:currencies,id|unique:portfolios,currency_id",
        ]);

        $portfolio = Portfolio::create($validated);
        alert()->success("success", "Currency added successfully to Portfolio");

        return redirect()->route("portfolio.index");
    }

    public function edit(Portfolio $portfolio)
    {
        return view("portfolio.edit", [
            "portfolio" => $portfolio->load("currency")
        ]);
    }

    public function update(Request $request, Portfolio $portfolio)
    {
        $validated = $request->validate([
            "currency_id" => "required|integer|exists:currencies,id|unique:portfolios,currency_id,".$portfolio->id,
        ]);

        $portfolio->update($validated);

        alert()->success("success", "Portfolio updated successfully");

        return redirect()->route('portfolio.index');
    }

    public function delete(Portfolio $portfolio)
    {
        return view("portfolio.delete", [
            "portfolio" => $portfolio->load("currency")
        ]);
    }

    public function destroy(Portfolio $portfolio)
    {
        $portfolio->delete();

        alert()->success("success", "Currency deleted successfully from Portfolio");

        return redirect()->route("portfolio.index");
    }

    public function refresh_rates()
    {
        CurrencyUpdater::update();

        alert()->success("success", "Rates successfully refreshed");

        return redirect()->route("portfolio.index");
    }
}
