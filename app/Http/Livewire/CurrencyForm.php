<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use App\Models\Portfolio;
use Livewire\Component;

class CurrencyForm extends Component
{
    public $currency;
    public $search;

    public function mount(Portfolio $portfolio = null, Currency $currency = null)
    {
        $this->search = "";
        $this->currency = null;

        if (isset($portfolio->id)) {
            $currency = $portfolio->currency;
            $this->currency = $currency->id;
        }

        if (isset($currency->id)) {
            $this->currency = $currency->id;
        }
    }

    public function render()
    {
        return view('livewire.currency-form', [
            "currencies" => Currency::search($this->search)->get()
        ]);
    }
}
