<?php

namespace App\Http\Livewire;

use App\Models\Currency;
use Livewire\Component;
use Livewire\WithPagination;

class CurrencyList extends Component
{
    use WithPagination;

    public $per_page;
    public $search;
    public $order_by;
    public $order_asc;

    public function updatingSearch()
    {
        $this->resetPage();
        $this->order_by = "name";
        $this->order_asc = true;
    }

    public function mount()
    {
        $this->per_page = 10;
        $this->search = "";
        $this->order_by = "name";
        $this->order_asc = true;
    }

    public function render()
    {
        return view('livewire.currency-list', [
            "currencies" => Currency::search($this->search)
            ->orderBy($this->order_by, $this->order_asc === true ? "asc" : "desc")
            ->simplePaginate($this->per_page)
        ]);
    }
}
