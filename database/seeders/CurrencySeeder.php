<?php

namespace Database\Seeders;

use App\Handlers\CurrencyDownloader;
use App\Handlers\CurrencyUpdater;
use App\Models\Currency;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CurrencyDownloader::download();
        CurrencyUpdater::update();
    }
}
