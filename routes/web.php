<?php

use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\PortfolioController;
use Illuminate\Support\Facades\Route;
use Swoole\Server\Port;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource("currencies", CurrencyController::class)->only(["index","store"]);
Route::resource("portfolio", PortfolioController::class)->except(["show"]);
Route::get("portfolio/{portfolio}/delete", [PortfolioController::class,"delete"])->name("portfolio.delete");
Route::get("portfolio/refresh", [PortfolioController::class, "refresh_rates"])->name("portfolio.refresh");
