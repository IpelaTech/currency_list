<div class="flex flex-col gap-4">
    <header class="flex flex-row gap-2 w-full">

        <section>
            <label class="">Search (name or code)</label>
            <input wire:model.debounce.300ms="search" class="input input-primary input-block" />
        </section>

        <section>
            <label> Per Page</label>
            <select class="select select-primary select-block" wire:model="per_page">
                <option>10</option>
                <option>25</option>
                <option>50</option>
            </select>
        </section>

        <section class="flex flex-row gap-2">
            <section>
                <label>Order By:</label>
                <select class="select select-primary select-block" wire:model="order_by">
                    <option value="name">Name</option>
                    <option value="code">Code</option>
                </select>
            </section>
            <section>
                <label>Direction</label>
                <select class="select select-primary select-block" wire:model="order_asc">
                    <option value="1">Ascending</option>
                    <option value="0">Descending</option>
                </select>
            </section>
        </section>
    </header>


    <table>
        <thead>
            <tr scope="row">
                <th scope="col">Name</th>
                <th scope="col">Code</th>
                <th scope="col">R1 buys</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($currencies as $currency)
                <tr scope="row">
                    <td data-label="Name">{{ $currency->name }}</td>
                    <td data-label="Code">{{ $currency->code }}</td>
                    <td data-label="Rate">{{ $currency->rate }} {{ \Illuminate\Support\Str::upper($currency->code) }}
                    </td>
                    <td data-label="Actions">
                        <section class="w-full flex flex-row gap-4 justify-evenly">
                            @if ($currency->portfolios->count() > 0)
                                <a class="flex gap-2 justify-evenly btn btn-error"
                                    href="{{ route('portfolio.delete', ['portfolio' => $currency->portfolios->first()]) }}">
                                    <x-lineawesome-times-circle-solid class="w-6 h-6" />
                                    <span>Remove from Portfolio</span>
                                </a>
                            @else
                                <a class="flex gap-2 justify-evenly btn btn-success"
                                    href="{{ route('portfolio.create', ['currency' => $currency->id]) }}">
                                    <x-lineawesome-plus-circle-solid class="w-6 h-6" />
                                    <span>Add To Portfolio</span>
                                </a>
                            @endif
                        </section>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {!! $currencies->links() !!}

</div>
