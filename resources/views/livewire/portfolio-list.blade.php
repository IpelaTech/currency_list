<div class="flex flex-col gap-4">
    <header class="flex flex-row gap-2 w-full">

        <section>
            <label class="">Search (name or code)</label>
            <input wire:model.debounce.300ms="search" class="input input-primary input-block" />
        </section>

        <section>
            <label> Per Page</label>
            <select class="select select-primary select-block" wire:model="per_page">
                <option>10</option>
                <option>25</option>
                <option>50</option>
            </select>
        </section>

        <section class="flex flex-row gap-2">
            <section>
                <label>Order By:</label>
                <select class="select select-primary select-block" wire:model="order_by">
                    <option value="name">Name</option>
                    <option value="code">Code</option>
                </select>
            </section>
            <section>
                <label>Direction</label>
                <select class="select select-primary select-block" wire:model="order_asc">
                    <option value="1">Ascending</option>
                    <option value="0">Descending</option>
                </select>
            </section>
        </section>
    </header>


    <table>
        <thead>
            <tr scope="row">
                <th scope="col">Name</th>
                <th scope="col">Code</th>
                <th scope="col">Rate</th>
                <th scope="col">Latest Rate Date</th>
                <th scope="col">Notes</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($portfolios as $portfolio)
                <tr scope="row">
                    <td data-label="Name">{{ $portfolio->currency->name }}</td>
                    <td data-label="Code">{{ $portfolio->currency->code }}</td>
                    <td data-label="Rate">{{ $portfolio->currency->rate }}</td>
                    <td data-label="Latest Rate Date">{{ $portfolio->currency->rate_date }}</td>
                    <td data-label="Notes">{{ $portfolio->notes }}</td>
                    <td data-label="Actions">
                        <section class="flex gap-4 items-center justify-evenly">
                            <a class="flex gap-2 items-center btn btn-warning"
                                href="{{ route('portfolio.edit', ['portfolio' => $portfolio]) }}">
                                <x-lineawesome-edit-solid class="w-6 h-6" />
                                <span>Edit</span>
                            </a>
                            <a class="flex gap-2 items-center btn btn-error"
                                href="{{ route('portfolio.delete', ['portfolio' => $portfolio]) }}">
                                <x-lineawesome-trash-solid class="w-6 h-6" />
                                <span>Delete</span>
                            </a>
                        </section>
                    </td>
                </tr>
            @empty
                <tr scope="row">
                    <td colspan="5">You do not have any currencies in your portfolio.</td>
                </tr>
            @endforelse
        </tbody>
    </table>

    {!! $portfolios->links() !!}


</div>
