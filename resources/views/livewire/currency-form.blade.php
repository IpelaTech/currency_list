<section class="flex flex-col gap-4 section">

    <section class="flex flex-col gap-4">
        <h5>Please select a currency from the dropdown below. You can search to narrow the results.</h5>
    </section>

    <section class="">
        <label>Search Currency (name or symbol)</label>
        <input class="input input-primary input-block" wire:model="search" />
    </section>

    <select class="select select-primary select-block" name="currency_id" wire:model="currency">
        <option value="">Please select a currency</option>
        @foreach ($currencies as $currency)
            <option value="{{ $currency->id }}">{{ $currency->name }} ({{ $currency->code }})</option>
        @endforeach
    </select>
    @error('currency_id')
        <section class="p-4 w-full bg-red-500 rounded">{{ $message }}</section>
    @enderror


    <section class="flex flex-row gap-4">
        <input type="submit" value="Save" class="btn btn-primary" />
        <a class="btn btn-error" href="{{ route('portfolio.index') }}">Cancel</a>
    </section>

</section>
