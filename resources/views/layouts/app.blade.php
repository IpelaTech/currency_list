<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    @vite('resources/css/app.css')
    @livewireStyles
</head>

<body class="antialiased min-h-screen flex flex-col gap-4 bg-gray-100">
    <header class="bg-white">
        <nav class="navbar">
            <section class="navbar-start">
                <a class="navbar-item flex gap-2 items-center" href="/">
                    <x-lineawesome-home-solid class="w-6 h-6" />
                    <span>Home</span>
                </a>
            </section>
            <section class="navbar-end">
                <a class="navbar-item flex gap-2 items-center" href="{{ route('currencies.index') }}">
                    <x-lineawesome-coins-solid class="w-6 h-6" />
                    <span>Currencies</span>
                </a>
                <a class="navbar-item flex gap-2 items-center" href="{{ route('portfolio.index') }}">
                    <x-lineawesome-wallet-solid class="w-6 h-6" />
                    <span>Portfolio</span>
                </a>
            </section>
        </nav>
    </header>
    <main class="container mx-auto py-8">
        @include('sweetalert::alert')
        @yield('content')
    </main>
    @livewireScripts
</body>

</html>
