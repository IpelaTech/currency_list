@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header>
            <h2>Edit Portfolio Currency</h2>
        </header>
        <form action="{{ route('portfolio.update', ['portfolio' => $portfolio]) }}" method="POST">
            @csrf
            @method('PATCH')
            @livewire('currency-form', ['portfolio' => $portfolio])
        </form>
    </section>
@endsection
