@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header>
            <h2>Add New Currency</h2>
        </header>

        <form action="{{ route('portfolio.store') }}" method="POST">
            @csrf

            @if (isset($currency))
                @livewire('currency-form', ['currency' => $currency])
            @else
                @livewire('currency-form')
            @endif
        </form>
    </section>
@endsection
