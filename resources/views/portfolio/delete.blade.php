@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header>
            <h2>Delete {{ $portfolio->currency->name }} {{ $portfolio->currency->code }}?</h2>
        </header>

        <section class="section flex flex-col gap-4">
            <p>Are you sure you want to delete? This is irreversible</p>
            <form action="{{ route('portfolio.destroy', ['portfolio' => $portfolio]) }}" method="POST"
                class="flex gap-2 items-center">
                @csrf
                @method('DELETE')
                <button type="submit" class="flex gap-2 items-center btn btn-primary">
                    <x-lineawesome-thumbs-up-solid class="w-6 h-6" />
                    <span>Yes please!</span>
                </button>
                <a class="btn btn-outline-error" href="{{ route('portfolio.index') }}">
                    <x-lineawesome-thumbs-down-solid class="w-6 h-6" />
                    <span>No, thanks</span>
                </a>
            </form>
        </section>
    </section>
@endsection
