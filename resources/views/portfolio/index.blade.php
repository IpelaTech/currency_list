@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header class="flex gap-4 justify-between">
            <h2>My Portfolio</h2>
            <section class="flex items-center gap-2">
                <a class="btn btn-secondary flex gap-2 items-center" href="{{ route('portfolio.create') }}">
                    <x-lineawesome-plus-circle-solid class="w-6 h-6" />
                    <span>Add New Currency</span>
                </a>
                <a class="btn btn-outline-secondary flex gap-2 items-center" href="{{ route('portfolio.refresh') }}">
                    <x-lineawesome-redo-alt-solid class="w-6 h-6" />
                    <span>Refresh Rates</span>
                </a>
            </section>
        </header>

        @livewire('portfolio-list')

    </section>
@endsection
