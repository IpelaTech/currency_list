@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header class="flex mx-auto">
            <h2>Welcome to Laravel Currencies List</h2>
            <figure>
                <img src="{{ asset('images/investment.svg') }}" />
            </figure>
        </header>

        <section class="flex flex-col gap-4">
            <header>
                <h3>Statistics</h3>
            </header>

            <section class="section flex flex-col gap-4">
                <header class="flex items-center justify-between">
                    <h3>Currencies</h3>
                    <a class="btn btn-primary flex gap-2 items-center" href="{{ route('currencies.index') }}">
                        <x-lineawesome-coins-solid class="w-6 h-6" />
                        <span>View Currencies</span>
                    </a>
                </header>

                <section class"">
                    <h4>Number of Currencies: {{ \App\Models\Currency::count() }}</h4>
                </section>

                <section class"">
                    <h4>Last Date Refreshed: {{ \App\Models\Currency::first()->rate_date->format('Y-m-d') }}</h4>
                </section>

            </section>

            <section class="section flex flex-col gap-4">
                <header class="flex items-center justify-between">
                    <h3>Portfolio</h3>
                    <a class="btn btn-primary flex gap-2 items-center" href="{{ route('portfolio.index') }}">
                        <x-lineawesome-wallet-solid class="w-6 h-6" />
                        <span>View Portfolio</span>
                    </a>
                </header>
                <section class="flex gap-4 items-center justify-between">
                    <section class="flex flex-col gap-4">
                        <h4>Number of Currencies in Portfolio: {{ \App\Models\Portfolio::count() }}</h4>
                        <h4>Last 5 Currencies added to Portfolio</h4>
                    </section>
                </section>

                <table>
                    <thead>
                        <tr scope="row">
                            <th scope="col">Name</th>
                            <th scope="col">Symbol</th>
                            <th scope="col">Date Added</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse(\App\Models\Portfolio::orderBy("date_created", "desc")->with(["currency"])->limit(5)->get() as $portfolio)
                            <tr scope="row">
                                <td data-label="Name">{{ $portfolio->currency->name }}</td>
                                <td data-label="Symbol">{{ $portfolio->currency->code }}</td>
                                <td data-label="Date Added">{{ $portfolio->created_at->format('Y-m-d') }}</td>
                            </tr>
                        @empty
                            <tr scope="row">
                                <td colspan="3">There are no currencies in your Portfolio</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </section>



        </section>

    </section>
@endsection
