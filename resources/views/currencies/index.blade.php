@extends('layouts.app')

@section('content')
    <section class="flex flex-col gap-4">
        <header class="flex flex-row gap-4 items-center justify-between">
            <section class="flex-col gap-4">
                <h2>Currency List</h2>
                <h3>Latest Date Updated: {{ $latest_date->format('Y-m-d') }}</h3>
            </section>
            <form action="{{ route('currencies.store') }}" method="POST">
                @csrf

                <button class="btn btn-outline-secondary flex gap-2 items-center" type="submit">
                    <x-lineawesome-redo-alt-solid class="w-6 h-6" />
                    <span>Refresh Rates</span>
                </button>

            </form>
        </header>

        @livewire('currency-list')
    </section>
@endsection
