<?php

namespace Tests\Feature;

use App\Handlers\CurrencyDownloader;
use App\Handlers\CurrencyUpdater;
use App\Models\Currency;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class CurrencyTest extends TestCase
{

    protected string $url;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan("migrate:fresh");
    }

    public function test_can_get_currency_list()
    {
        $url = config("currency.url_list");
        Http::fake([
            $url => Http::response([
                "cr1" => "Currency 1",
                "cr2" => "Currency 2"
            ], 200),
        ]);

        $currencies = CurrencyDownloader::download();

        $this->assertCount(2, Currency::all());
        $this->assertDatabaseHas("currencies", [
            "code" => "cr1",
        ]);
        $this->assertDatabaseHas("currencies", [
            "code" => "cr2"
        ]);
    }

    public function test_can_update_currency_rates()
    {
        $url_list = config("currency.url_list");
        $url_zar = config("currency.url_zar");
        Http::fake([
            $url_list => Http::response([
                "cr1" => "Currency 1",
                "cr2" => "Currency 2"
            ], 200),

            $url_zar => Http::response([
                "date" => now(),
                "zar" => [
                    "cr1" => 2.2,
                    "cr2" => 0.55
                ]
            ], 200)
        ]);

        CurrencyDownloader::download();
        CurrencyUpdater::update();

        $this->assertDatabaseHas("currencies", [
            "code" => "cr1",
            "rate" => 2.2,
        ]);
        $this->assertDatabaseHas("currencies", [
            "code" => "cr2",
            "rate" => 0.55,
        ]);
    }
}
