<?php

namespace Tests\Feature;

use App\Handlers\CurrencyDownloader;
use App\Handlers\CurrencyUpdater;
use App\Models\Currency;
use App\Models\Portfolio;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class PortfolioTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan("migrate:fresh");
    }


    public function test_can_add_new_currency_to_portfolio()
    {
        $currency =  Currency::create([
            "code" =>"cr3",
            "name" => "Currency 3"
        ]);

        $response = $this->post(route('portfolio.store'), [
            "currency_id" => $currency->id,
        ]);

        $this->assertDatabaseHas("portfolios", [
            "currency_id" => $currency->id
        ]);

        //check relationship
        $portfolio = Portfolio::first();
        $portfolio_currency = Currency::where("id", $portfolio->currency_id)->first();
        $this->assertEquals($currency->id, $portfolio_currency->id);
    }

    public function test_cannot_add_empty_currency()
    {
        $response = $this->post(route('portfolio.store'), []);
        $response->assertInvalid([
            "currency_id"=> "The currency id field is required."
        ]);
    }

    public function test_cannot_add_duplicate_currency_to_portfolio()
    {
        $currency =  Currency::create([
            "code" =>"cr3",
            "name" => "Currency 3"
        ]);

        $portfolio = Portfolio::create([
            "currency_id" => $currency->id
        ]);

        $response = $this->post(route('portfolio.store'), [
            "currency_id" => $currency->id
        ]);

        $response->assertInvalid([
            "currency_id"=> "The currency id has already been taken."
        ]);
    }

    public function test_can_edit_existing_portfolio()
    {
        $currency1 =  Currency::create([
            "code" =>"cr3",
            "name" => "Currency 3"
        ]);

        $currency2 = Currency::create([
            "code" => "cr1",
            "name" => "Currency 1"
        ]);

        $portfolio = Portfolio::create([
            "currency_id" => $currency1->id
        ]);

        $response = $this->patch(route('portfolio.update', ['portfolio' => $portfolio]), [
            "currency_id" => $currency2->id,
            "notes" => "new notes"
        ]);

        $this->assertDatabaseHas("portfolios", [
            "currency_id" => $currency2->id
        ]);

        $this->assertDatabaseMissing("portfolios", [
            "currency_id" => $currency1->id
        ]);
    }
}
